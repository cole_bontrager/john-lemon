using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarLight : MonoBehaviour
{
    [SerializeField] GameObject headlight;
    [SerializeField] GameObject headLightAlert;
    [SerializeField] float headlightDrainRate;
    [SerializeField] Image batteryBar;
    private float batteryLevel = 100f;
    [SerializeField] float batteryPickupAmount;
    [SerializeField] GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        headlight.SetActive(false);
        headLightAlert.SetActive(false);
        batteryBar.type = Image.Type.Filled;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameManager.GetState() == GameManager.status.play)
        {
            if (Input.GetKeyDown(KeyCode.LeftShift) && batteryLevel > 0)
            {
                headlight.SetActive(!headlight.activeSelf);
                headLightAlert.SetActive(!headLightAlert.activeSelf);
            }

            if (headlight.activeSelf)
            {
                batteryLevel -= headlightDrainRate * Time.deltaTime;
            }

            if (batteryLevel <= 0)
            {
                batteryLevel = 0;
                headlight.SetActive(false);
            }

            
        }
        else
        {
            batteryLevel = 100;
        }
        batteryBar.fillAmount = batteryLevel / 100;


    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Battery")
        {
            batteryLevel = Mathf.Min(batteryLevel + batteryPickupAmount, 100f);
            Destroy(other.gameObject);
        }

    }
}
