using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    [SerializeField] Transform[] positions;
    [SerializeField] float speed;
    [SerializeField] float chaseSpeed;
    [SerializeField] Transform player;
    [SerializeField] float abandonChaseRange;
    [SerializeField] float abandonChaseTime;
    private float chaseTimer = 0;
    private int curPos = 0;
    private bool chasing = false;
    // Start is called before the first frame update
    void Start()
    {
        if (positions != null && positions.Length > 0)
            transform.position = positions[0].position;
    }

    // Update is called once per frame
    void Update()
    {
        if(chasing)
        {
            if((player.position - transform.position).magnitude > abandonChaseRange || chaseTimer > abandonChaseTime || player.GetComponent<CarController>().cloaked)
            {
                chasing = false;
                chaseTimer = 0;
            }
            else
            {
                Vector3 pred = player.position + player.GetComponent<CarController>().rb.velocity * .2f;
                Vector3 dir = pred - transform.position;
                transform.position += dir * chaseSpeed * Time.deltaTime;
                transform.LookAt(player);
                chaseTimer += Time.deltaTime;
            }
        }
        else if(positions != null && positions.Length > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, positions[curPos].position, speed);
            transform.LookAt(positions[curPos]);
            if((positions[curPos].position - transform.position).magnitude < .02)
            {
                curPos = curPos == positions.Length - 1 ? 0 : curPos+1;
            }
        }
    }

    public void StartChase()
    {
        chasing = true;
    }
}
