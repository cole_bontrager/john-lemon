using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject sphere;
    [SerializeField] GameObject startUI;
    [SerializeField] GameObject endUI;
    [SerializeField] TextMeshProUGUI finishText;
    [SerializeField] TextMeshProUGUI timeText;
    [SerializeField] CarController carController;
    private Vector3 spawnPos;
    private float time = 0f;
    private static bool reload = true;
    public enum status
    {
        start,
        play,
        finished
    }
    private status state = status.start;

    // Start is called before the first frame update
    void Start()
    {
        if(reload)
        {
            reload = false;
            SceneManager.LoadScene(0);
        }
        spawnPos = sphere.transform.position;
        Reset();
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            Reset();
        }
        if(state == status.play)
        {
            time += Time.deltaTime;
        }
        timeText.text = time.ToString("0.##");

        

        if(Input.GetKey(KeyCode.Space) && state == status.start)
        {
            startUI.SetActive(false);
            carController.canMove = true;
            state = status.play;
        }
        if (Input.GetKey(KeyCode.Space) && state == status.finished)
        {
            SceneManager.LoadScene(0);
        }
    }

    public void Reset()
    {
        time = 0f;
        sphere.transform.position = spawnPos;
        sphere.GetComponent<Rigidbody>().velocity = Vector3.zero;
        startUI.SetActive(true);
        carController.ResetRot();
        carController.canMove = false;
        state = status.start;

    }

    public void Finish()
    {
        carController.canMove = false;
        finishText.text = "You finished in " + time.ToString("0.##") + " seconds. Press SPACE to reset.";
        endUI.SetActive(true);
        state = status.finished;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Reset")
        {
            SceneManager.LoadScene(0);
        }
        else if(other.tag == "Finish")
        {
            Finish();
        }
    }

    public status GetState()
    {
        return state;
    }
}
