using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    [SerializeField] public Rigidbody rb;
    private float moveInput;
    private float turnInput;
    private bool grounded;

    [SerializeField] float rotSpeed;
    [SerializeField] float airDrag;
    [SerializeField] float groundDrag;
    [SerializeField] float speed;
    [SerializeField] float reverseSpeed;
    [SerializeField] float turnSpeed;
    [SerializeField] LayerMask groundLayer;
    public bool canMove = false;
    public bool cloaked = false;
    private Quaternion startRot;
    // Start is called before the first frame update
    void Start()
    {
        rb.transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = Input.GetAxisRaw("Vertical");
        moveInput *= moveInput > 0 ? speed : reverseSpeed;
        
        transform.position = rb.transform.position;

        turnInput = Input.GetAxisRaw("Horizontal");
        float rot = turnInput * turnSpeed * Time.deltaTime * Input.GetAxisRaw("Vertical");

        if(canMove)
        {
            transform.Rotate(0, rot, 0, Space.World);
        }
        

        RaycastHit hit;
        grounded = Physics.Raycast(transform.position, -Vector3.up, out hit, 1f, groundLayer);
        if(grounded)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation, rotSpeed * Time.deltaTime);
        }
        

        rb.drag = grounded ? groundDrag : airDrag;
    }

    public void ResetRot()
    {
        transform.rotation = startRot;
    }

    private void FixedUpdate()
    {
        if(grounded && canMove)
        {
            rb.AddForce(transform.forward * moveInput * speed, ForceMode.Acceleration);
        }
        else
        {
            rb.AddForce(-transform.up * 9.8f, ForceMode.Acceleration);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if(other.tag == "Bush")
        {
            Debug.Log("cloak");
            cloaked = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Bush")
        {
            Debug.Log("uncloak");
            cloaked = false;
        }
    }
}
