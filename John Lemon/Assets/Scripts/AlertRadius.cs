using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertRadius : MonoBehaviour
{
    [SerializeField] CarController carController;
    private void OnTriggerEnter(Collider other)
    {
        Ghost ghost = other.gameObject.GetComponent<Ghost>();
        if (ghost != null && !carController.cloaked)
        {
            ghost.StartChase();
        }
    }
}
